import java.util.ArrayList;
import java.util.List;

public class History {

    private static final History chatHistory = new History();
    private History() {}
    public static History getInstance() {
        return chatHistory;
    }

    private List<String> entries = new ArrayList<>();
    private List<CommandInterpreter> interpreters = new ArrayList<>();

    // Returns a list of active users
    public List<String> getActiveUsers() {
        List<String> activeUsers = new ArrayList<>();
        // For each of the commandInterpreters
        for (CommandInterpreter i : this.interpreters) {
            // Get the userName and add it to the list
            activeUsers.add(i.getUserName());
        }
        return activeUsers;
    }

    // Add an entry to the chat history and broadcast to other users
    public void addEntry(String str, CommandInterpreter calledBy) {
        // Add entry to ArrayList
        entries.add(str);
        // For each cI (one for each user)
        for (CommandInterpreter i : interpreters) {
            // If the cI is NOT the onw that added the entry
            if (!i.equals(calledBy)) {
                // Call the cI's historyChanged() method
                i.historyChanged(str);
            }
        }
    }

    // Add an interpreter to the list
    public void addInterpreter(CommandInterpreter interpreter) {
        interpreters.add(interpreter);
    }

    // Return a litst of all stored entries
    public List<String> getHistory() {
        return this.entries;
    }
}