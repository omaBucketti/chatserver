// Interface between chatHistory and cI
public interface ChatObserver {

    void historyChanged(String entry);
}
