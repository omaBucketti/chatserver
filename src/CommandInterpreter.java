import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

public class CommandInterpreter implements ChatObserver, Runnable {

    private Socket s;
    private History chatHistory;
    private InputStreamReader serverInput;
    private BufferedReader serverBuffer;
    private PrintWriter out;
    private String userName;
    private HashMap<String, Integer> commands;

    // Constructor
    public CommandInterpreter(Socket s, History chatHistory) {
        this.s = s;
        this.chatHistory = chatHistory;
        // Default userName may be overwritten
        this.userName = "Anon";
        this.commands = new HashMap<>();
        // The commands
        commands.put(":quit", 0);
        commands.put(":exit", 0);
        commands.put(":list", 1);
        commands.put(":help", 1);
        commands.put(":users", 2);
        commands.put(":who", 2);
        try {
            this.serverInput = new InputStreamReader(s.getInputStream());
            this.serverBuffer = new BufferedReader(serverInput);
            this.out = new PrintWriter(s.getOutputStream(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void historyChanged(String entry) {
        out.println(entry);
    }

    @Override
    public void run() {
        try {

            // Print greeting and all chat history when the user ser joins the room
            out.println("Please enter your name:");
            out.println("");


            // Get the users name from user
            String user = serverBuffer.readLine();
            // If any name is provided
            if (user.length() > 0) {
                // Overwrite userName
                userName = user;
            }

            out.println("");
            out.println("Hi " + userName + ", you're connected on port " + s.getPort());
            out.println("");
            // Notify other users that user has joined
            chatHistory.addEntry(userName + " JOINED", this);
            // Print out the chat history for new connected users
            for (String hEntry : chatHistory.getHistory()) {
                out.println(hEntry);
            }
            // Keep listening for input
            while (true) {
                String entry;
                while((entry = serverBuffer.readLine()) != null) {
                    if (commands.get(entry) == null) {
                        // Add it to the chat history
                        chatHistory.addEntry(userName + ": " + entry, this);
                    } else {
                        parseCommand(entry);
                    }
                }
                // If client has disconnected
                if (serverBuffer.read() == -1) {
                    chatHistory.addEntry(userName + " DISCONNECTED", this);
                    // Notify and exit the loop
                    System.out.print("User disconnected");
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Returns the name of the user to whom this cI belongs
    public String getUserName() {
        return this.userName;
    }

    // Executes a command based on certain input from user
    private void parseCommand (String command) {
        switch (commands.get(command)) {
            case 0:
                // Disconnect from the server
                try {
                    s.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 1:
                // List possible commands
                out.println(":list OR :help >> DISPLAY COMMANDS");
                out.println(":users OR :who >> SHOW CONNECTED USERS");
                out.println(":exit OR :quit >> DISCONNECT FROM SERVER");
                break;
            case 2:
                // List current active users
                for (String user : chatHistory.getActiveUsers()) {
                    out.println(user);
                }
        }
    }
}