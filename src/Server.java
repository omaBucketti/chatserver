import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws IOException {

        // Creating a listening socket on port 1024 with backlog 20
        ServerSocket listener = new ServerSocket(1024, 20);

        while (true) {
            try {
                // Creates a new socket on a random free port
                Socket socket = listener.accept();
                // Create cI instance for the new thread
                CommandInterpreter cI = new CommandInterpreter(socket, History.getInstance());
                History.getInstance().addInterpreter(cI);
                Thread thread = new Thread(cI);
                thread.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}